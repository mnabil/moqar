#!/bin/bash

sudo apt-get -y update
sudo apt-get -y install python-dev libpq-dev python-pip build-essential
sudo pip install --upgrade pip
sudo pip install virtualenvwrapper

if [ ! -f /home/vagrant/.install_extra_packages ]
then
    TEXT_TO_ECHO='export WORKON_HOME=$HOME/.virtualenvs\nsource /usr/local/bin/virtualenvwrapper.sh'
    echo -e $TEXT_TO_ECHO >> /home/vagrant/.bashrc
    su - vagrant -c "source `which virtualenvwrapper.sh` && mkvirtualenv -a /vagrant/moqar -r /vagrant/requirements.txt -v --no-site-packages moqar_env"
    touch /home/vagrant/.install_extra_packages
fi
