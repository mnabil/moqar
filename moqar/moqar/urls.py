from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from moqar_app.api import moqarUserEntry
from moqar_app.api import moqsEntry
from moqar_app.api import moqarUserGroupEntry
from moqar_app.api import moqarCommentResource
from moqar_app.api import moqarTagResource
from moqar_app.api import moqarImageResource
from moqar_app.api import userResource
from moqar_app.api import moqarAutoSearchResource
from moqar_app.api import moqarFullTextSearchResource
from moqar_app.api import moqarInvitationCodeResource
from moqar_app.views import * # FIXME
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# user urls
user = userResource()
# Moar users urls
moqar_user_entry = moqarUserEntry()
# Moqs urls
moqs_entry = moqsEntry()
# Moqar user groups urls
moqarug_entry = moqarUserGroupEntry()
# Moqar comment urls
moqar_comment_resource = moqarCommentResource()
# Moqar tag urls
moqar_tag_resource = moqarTagResource()
# Moqar image urls
moqar_image_resource = moqarImageResource()
# Moqar auto search url
moqar_auto_search_resource = moqarAutoSearchResource()
# Moqar full test search url
moqar_full_search_resource = moqarFullTextSearchResource()
# Moqar invitation code resource
moqar_invitation_code_resource = moqarInvitationCodeResource()

urlpatterns = patterns('',
                       # Examples:
                       #url(r'^$', home, name='home'),
                       # url(r'^moqar/', include('moqar.urls')),

                       #url(r'^admin/', include(admin.site.urls)),
                       #url(r"^accounts/email/$", home, name='home'),
                       #url(r'^accounts/', include('allauth.urls')),
                       #url(r'^signup/$', signup, name='signup'),
                       #url(r'^finish/$', finish, name='finish'),
                       #url(r'^api/', include(user.urls)),
                       #url(r'^api/', include(moqar_user_entry.urls)),
                       #url(r'^api/', include(moqs_entry.urls)),
                       #url(r'^api/', include(moqarug_entry.urls)),
                       #url(r'^api/', include(moqar_comment_resource.urls)),
                       #url(r'^api/', include(moqar_tag_resource.urls)),
                       #url(r'^api/', include(moqar_image_resource.urls)),
                       #url(r'^api/', include(moqar_auto_search_resource.urls)),
                       #url(r'^api/', include(moqar_full_search_resource.urls)),
                       url(r'^api/', include(moqar_invitation_code_resource.urls)),
                       url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico')),
                       # (elleestcrimi): The next line is needed for angular to work correctly
                       url(r'^$', home, name='home'),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
