#from django.utils.translation import ugettext
from django.http import HttpResponseRedirect
#from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist

from social_auth.models import UserSocialAuth
#from social_auth.exceptions import AuthAlreadyAssociated

from moqar_app.models import MoqarUser


def create_user(backend, details, response, uid, username, user=None, *args,
                **kwargs):
    """Create user. Depends on get_username pipeline."""
    print "**************"
    if user:
        print "userrrr"
        return {'user': user}
    if not username:
        print "not username"
        return None

    print "3adeena"
    print "rrrr: %s" % response
    # Avoid hitting field max length
    email = details.get('email')
    original_email = None
    if email and UserSocialAuth.email_max_length() < len(email):
        original_email = email
        email = ''

    user = UserSocialAuth.create_user(username=username, email=email)

    location = response.get('location', None)
    print "llll: %s" % location
    if location:
        location = location.get('name', None)

    print "locccc: %s" % location
    print type(location)
    moqar_user = MoqarUser(
        user,
        gender=response.get("gender", "male"),
        date_of_birth=response.get("birthday", "1980-1-1"),
        location=location)
    moqar_user.save()
    print "afffff"
    return {
        'user': user,
        'original_email': original_email,
        'is_new': True
    }


def redirect_to_form(*args, **kwargs):
    print "will redirect"
    print "args: %s" % str(args)
    print "ssss redirect"
    print "kwargs: %s" % kwargs
    response = kwargs.get('response', None)
    username = response.get('username', None)
    email = response.get('email', None)
    if not kwargs['request'].session.get('saved_username') and \
       kwargs.get('user') is None:
        return HttpResponseRedirect('/form/?username=%s&email=%s' % (username, email))
