# -*- coding: utf-8 -*-
from django.contrib import admin
from moqar_app.models import MoqarUser, Moq, MoqarUsersGroup, Comment, MoqImage, Tag, InvitationCodes

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin


class MoqarUserInline(admin.StackedInline):
    model = MoqarUser
    can_delete = False


class UserAdmin(UserAdmin):
    inlines = (MoqarUserInline,)


class GroupAdmin(admin.ModelAdmin):
    pass


class InvitationCodesAdmin(admin.ModelAdmin):
    list_display = ('email', 'invitation_code', 'expired')


admin.site.register(Comment)
admin.site.register(Tag)
admin.site.register(MoqImage)
admin.site.register(Moq)
admin.site.register(InvitationCodes, InvitationCodesAdmin)
admin.site.unregister(User)
admin.site.register(MoqarUsersGroup)
admin.site.register(User, UserAdmin)
