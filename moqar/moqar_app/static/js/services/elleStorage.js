/*global moqar */
'use strict';

/**
 * Services that persists and retrieves TODOs from localStorage
 */
moqar.

factory('elleStorage', function () {
	return {
		get: function (id) {
			return JSON.parse(localStorage.getItem(id) || '[]');
		},

		put: function (id, obj) {
			localStorage.setItem(id, JSON.stringify(obj));
		}
	};
});