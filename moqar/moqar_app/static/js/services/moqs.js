moqar.factory('Moqs', function ($resource, elleStorage) {

  var moqsResource =  $resource('/api/moqs\\/', {format: 'json'});

  return {
    all: function(username) {
     return moqsResource.all()
    }
  }
});