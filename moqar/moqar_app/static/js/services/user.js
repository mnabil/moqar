moqar.factory('User', function ($resource, elleStorage) {

  var userResource =  $resource('/api/users/:username\\/', {format: 'json'});

  return {
    get: function(username) {

      user = elleStorage.get(username);

      if(!user.length) {
        user = userResource.get({username: username})
        elleStorage.put(username, user)
        return user
      } else 
        return user
    }
  }
});