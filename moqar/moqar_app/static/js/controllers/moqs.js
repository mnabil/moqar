'use strict';

moqar.controller('Moqs', function Moqs($scope, $http) {
    var width = $(document).width() 
    var factor = 100
    var fixHeight = function(){ 
        if( $(document).width() <= width) {
            $('.moqs').height( $('.moqs').height() - factor)
           fixHeight();
        } else {
            $('.moqs').height( $('.moqs').height() + factor)
        }
    }
    $http.get('/api/moqs/?format=json').success(function(data) {
        $scope.moqs = data.objects
        setTimeout(function(){
            fixHeight();
        }, 1)
    });
});