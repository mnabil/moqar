'use strict';

moqar.controller('application', function user($scope, $http, $timeout) {
	$scope.autocomplete = function() {
		if($scope.query.trim().length < 3) return
		$scope.state = 'loading';
		$timeout.cancel($scope.to_autocomplete);
		$scope.to_autocomplete = $timeout(function(){
		    $http.post('/api/autosearch/?format=json', {'auto_search_string': $scope.query })
		    .success(function(data) {
		        $scope.results = data.moqs
		       	$scope.state = '';
		       	$scope.no_results = !data.moqs.length
		    });
		}, 200)
	}
});
