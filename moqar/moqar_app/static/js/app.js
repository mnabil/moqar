/*global angular */
/*jshint unused:false */
'use strict';

/**
 * The main moqar app module
 *
 * @type {angular.Module}
 */
var moqar = angular.module('moqar', ['ui.state']).
config(function($stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider.
    state('home', {
        url: "/",
        views: {
            "main": {
                templateUrl: "/static/moqs/index.html"
            }
        }
    }).
    state('users', {
        url: "/users/:username/",
        views: {
            "main": {
                templateUrl: "/static/users/show.html"
            }
        }
    }).
    state('users.moqs', {
        url: "moqs/", 
        views: {
            "user_sub": {
                templateUrl: "/static/moqs/index.html"
            }
        }
    }).
    state('moq', {
        url: "/moq/:id/",
        views: {
            "main": {
                templateUrl: "/static/moqs/show.html"
            }
        }
    }).
    state('new_moq', {
        url: "/moqs/new/",
        views: {
            "main": {
                templateUrl: "/static/moqs/new.html"
            }
        }
    });
}).
config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});