moqar.directive("keyboardize", function(){
  return function (scope, element, attrs) {
    element.ready(function() {
      scope.focusIndex = 0;
      scope.keys = {
        13: function(children) { 
          angular.element(children[scope.focusIndex]).find('a').click();
          scope.keys.cancel();
        },
        27: function(children) {
          scope.keys.cancel();
        },
        38: function(children) { scope.focusIndex--; },
        40: function(children) { scope.focusIndex++; },
        cancel: function() {
          scope.query = ''
          scope.results = []
        }
      };

      $(document).keydown(function(e) {
        if(!element.is(':visible')) return;
        var children = element.find('[ng-repeat]')
        var key = e.which
        scope.keys[e.which](children);
        if(scope.focusIndex < 0) scope.focusIndex = children.length -1;
        if(scope.focusIndex >= children.length) scope.focusIndex = 0;

        scope.$apply();
      });
    })
  }  
});