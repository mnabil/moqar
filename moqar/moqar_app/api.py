""" REST api for moqar model """
from django.conf.urls import url
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, Resource
from tastypie import fields
from tastypie.authentication import Authentication
from moqar_app.models import User
from moqar_app.models import MoqarUser
from moqar_app.models import Moq
from moqar_app.models import MoqarUsersGroup
from moqar_app.models import Comment
from moqar_app.models import Tag
from moqar_app.models import MoqImage
from moqar_app.models import InvitationCodes
from haystack.query import SearchQuerySet
from random import choice
import string
from django.core.validators import validate_email
from django.http import HttpResponse
from tastypie.http import HttpAccepted
from tastypie.exceptions import ImmediateHttpResponse


class userResource(ModelResource):

    """ Django user resource """
    moqar_user = fields.ToOneField('moqar_app.api.moqarUserEntry', 'moqaruser')

    class Meta:
        queryset = User.objects.all()
        detail_uri_name = 'username'
        resource_name = 'users'
        excludes = ['password', 'is_superuser', 'is_active', 'is_staff']
        # query the users using username
        """
        Example :
                http://127.0.0.1:8888/api/users/nabil1/?format=json
        """

        def prepend_urls(self):
            return [
                url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view(
                    'dispatch_detail'), name="api_dispatch_detail"),
            ]


class moqarUserEntry(ModelResource):

    """ Moqar user entry"""
    moqs = fields.ToManyField('moqar_app.api.moqsEntry', 'moq_set')
    user = fields.ToOneField('moqar_app.api.userResource', 'user')
    follows = fields.ToManyField('moqar_app.api.moqarUserEntry', 'follows')

    class Meta:
        queryset = MoqarUser.objects.all()
        resource_name = 'moqarusers'


class moqsEntry(ModelResource):

    """ Moq entry """
    # the user id Resource field
    user = fields.ToOneField(moqarUserEntry, 'moqar_user')
    images = fields.ToManyField(
        'moqar_app.api.moqarImageResource', 'moqimage_set')

    class Meta:
        queryset = Moq.objects.all()
        resource_name = 'moqs'
        include_resource_uri = False
        authorization = Authorization()
        """
        Testing put data with curl:
            curl --dump-header - -H "Content-Type: application/json" -X POST --data 
            '{"question": "is that a successful rest test?", "images": [], "user" : "/api/moqarusers/1/" }' 
            http://127.0.0.1:8888/api/moqs/
        """

    def dehydrate(self, bundle):
        # dehydrate the moqs entries to add the user info and images paths
        bundle.data['user'] = {
            'username': bundle.obj.moqar_user.user.username,
            'profile_picture': bundle.obj.moqar_user.profile_picture,
            'id': bundle.obj.moqar_user.id
        }
        bundle.data['images'] = [
            image for image in bundle.obj.moqimage_set.values()]

        return bundle


class moqarUserGroupEntry(ModelResource):

    """ Moqar user group entry """
    # members = fields.ListField()
    users = fields.ToManyField(moqarUserEntry, 'moqar_users')
    created_by = fields.ToOneField(moqarUserEntry, 'created_by')

    class Meta:
        queryset = MoqarUsersGroup.objects.all()
        resource_name = 'moqarug'
        include_resource_uri = False
        authorization = Authorization()
        """
        Testing put data with curl:
            curl --dump-header - -H "Content-Type: application/json" -X POST --data '{"name": "REST_TEST_GROUP", \ 
            "users": ["/api/moqarusers/1/", "/api/moqarusers/2/"], "created_by": "/api/moqarusers/2/" }' \
            http://127.0.0.1:8888/api/moqarug/ 
        """


class moqarCommentResource(ModelResource):

    """ Moqar comment resource """
    written_by = fields.ToOneField(moqarUserEntry, 'written_by')
    image = fields.ToOneField('moqar_app.api.moqarImageResource', 'moq_image')

    class Meta:
        queryset = Comment.objects.all()
        resource_name = 'comments'
        include_resource_uri = False
        authorization = Authorization()
        """
        Testing put data with curl:
            curl --dump-header - -H "Content-Type: application/json" -X POST --data \
            '{"text": "REST test", "written_by": "/api/moqarusers/1/", "image" : "/api/moqimgs/1/"}' \
            http://127.0.0.1:8888/api/comments/
        """


class moqarTagResource(ModelResource):

    """ Moqar tag resource """
    describes = fields.ToManyField('moqar_app.api.moqsEntry', 'describes')
    followed_by = fields.ToManyField(
        'moqar_app.api.moqarUserEntry', 'followed_by')

    class Meta:
        queryset = Tag.objects.all()
        resource_name = 'tags'
        include_resource_uri = False
        authorization = Authorization()
        """
        Testing put data with curl :
             curl --dump-header - -H "Content-Type: application/json" -X POST --data \
             '{"text": "tag1"}' http://127.0.0.1:8888/api/tags/
        """


class moqarImageResource(ModelResource):

    """ Moqar image resource """
    comments = fields.ToManyField(
        'moqar_app.api.moqarCommentResource', 'comment_set')
    moq = fields.ToOneField('moqar_app.api.moqsEntry', 'moq')

    class Meta:
        queryset = MoqImage.objects.all()
        resource_name = 'moqimgs'
        include_resource_uri = False
        authorization = Authorization()
        """
        Testing put data with curl :
            curl --dump-header - -H "Content-Type: application/json" -X POST --data \
            '{"comments": [], "description": "REST create moq img test", "image" : \
            "media/media/326802_3146309950488_190915492_o.jpg" \
            , "image_number": "1", "rating":"0.0", "moq": "/api/moqs/1/"}' \
            http://127.0.0.1:8888/api/moqimgs/ 
        """


class moqarAutoSearchResource(Resource):

    """ 
    Moqar Autosearch Resource 
    Results are now limted to 5 results
    """
    class Meta:
        resource_name = 'autosearch'
        authentication = Authentication()
        authorization = Authorization()
        include_resource_uri = False
        always_return_data = True
        list_allowed_methods = ['get']
    search_query_set = SearchQuerySet()
    """ 
    Testing example : 
        http://127.0.0.1:8888/api/autosearch/?q=ha
    """

    def dehydrate(self, bundle):
        # Matching tags
        search_string = bundle.request.GET['q']
        bundle.data['tags'] = [
            {'id': tag_obj.object.id,
             'text': tag_obj.object.text} for tag_obj in
            self.search_query_set.autocomplete(tag_text_auto=search_string)]
        # Matching Users
        bundle.data['users'] = [
            {'username': user_obj.object.user.username,
             'profile_picture': user_obj.object.profile_picture,
             'id': user_obj.object.id} for user_obj in
            self.search_query_set.autocomplete(user_name_auto=search_string)]

        bundle.data['moqs'] = [
            {'image': moq_obj.object.moqimage_set.values()[0]['image'],
             'question': moq_obj.object.question,
             'id': moq_obj.object.id} for moq_obj in
            self.search_query_set.autocomplete(moq_question_auto=search_string)]
        return bundle

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        return kwargs

    def get_object_list(self, request):
        """ tastypie get_object_list """
        # TODO : Fix this in a cleaner way
        return '.'

    def obj_get_list(self, request=None, **kwargs):
        """ tastypie obj_get_list """
        return self.get_object_list(request)


class moqarFullTextSearchResource(Resource):

    """ 
    Moqar fullsearch Resource 
    
    """
    class Meta:
        resource_name = 'fullsearch'
        authentication = Authentication()
        authorization = Authorization()
        include_resource_uri = False
        always_return_data = True
        list_allowed_methods = ['get']
    search_query_set = SearchQuerySet()
    """ 
    Testing example : 
        http://127.0.0.1:8888/api/autosearch/?q=ha
    """

    def dehydrate(self, bundle):
        # Matching tags
        search_string = bundle.request.GET['q']
        bundle.data['tags'] = [
            {'id': tag_obj.object.id,
             'text': tag_obj.object.text} for tag_obj in
            self.search_query_set.autocomplete(tag_text_auto=search_string)[:5]]
        # Matching Users
        bundle.data['users'] = [
            {'username': user_obj.object.user.username,
             'profile_picture': user_obj.object.profile_picture,
             'id': user_obj.object.id} for user_obj in
            self.search_query_set.autocomplete(user_name_auto=search_string)[:5]]

        bundle.data['moqs'] = [
            {'image': moq_obj.object.moqimage_set.values()[0]['image'],
             'question': moq_obj.object.question,
             'id': moq_obj.object.id} for moq_obj in
            self.search_query_set.autocomplete(moq_question_auto=search_string)[:5]]
        return bundle

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        return kwargs

    def get_object_list(self, request):
        """ tastypie get_object_list """
        # TODO : Fix this in a cleaner way
        return '.'

    def obj_get_list(self, request=None, **kwargs):
        """ tastypie obj_get_list """
        return self.get_object_list(request)


class moqarInvitationCodeResource(ModelResource):

    """ Invitation code resource """
    invitation_code = fields.CharField(attribute='invitation_code')
    email = fields.CharField(attribute='email', null=False)
    expired = fields.BooleanField(attribute='expired')

    class Meta:
        queryset = InvitationCodes.objects.all()
        resource_name = 'icode'
        include_resource_uri = False
        authorization = Authorization()
        allowed_methods = ['post']
	list_allowed_methods = ['post']
	detail_allowed_methods = ['post']
	"""
        Testing put data with curl:
            curl --dump-header - -H "Content-Type: application/json" -X POST --data \
            '{"email":"m.nabil.hafez@gmail.com"}' http://127.0.0.1:8888/api/icode/
        """

    def hydrate(self, bundle):
        """ Populate the invitation_code field """
        try:
            validate_email(bundle.data['email'])
        except Exception, ex:
            raise ImmediateHttpResponse(
                HttpAccepted("Ouch!! Does that look like a real email to you? Punk."))
        email_exists = InvitationCodes.objects.filter(
            email=bundle.data['email']).exists()
        if email_exists:
            raise ImmediateHttpResponse(
                HttpAccepted(
                    "Oops! An account has already been requested with this email.")
            )
        if len(bundle.data['email']) > 254:
            raise ImmediateHttpResponse(
                HttpAccepted("Ouch!! Does that look like a real email to you ? ... Punk."))
        bundle.data['invitation_code'] = "".join(
            [choice(string.ascii_letters + string.digits) for num in xrange(80)])
        return bundle
