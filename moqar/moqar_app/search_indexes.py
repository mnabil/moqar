from haystack import indexes
from moqar_app.models import Moq
from moqar_app.models import MoqarUser
from moqar_app.models import Tag

"""
Note : you can test it by using : 
     sqs = SearchQuerySet()
     sqs.autocomplete(moqar_question_auto='test')
"""


class MoqIndex(indexes.SearchIndex, indexes.Indexable):

    """ Moq search index """
    text = indexes.CharField(
        document=True, use_template=False, model_attr='question')
    moq_question_auto = indexes.EdgeNgramField(model_attr='question')
    moqar_user = indexes.CharField(model_attr='moqar_user')

    def get_model(self):
        """ Get model method """
        return Moq

    def index_queryset(self, using=None):
        """
        Index query method
        Used when the entire index for model is updated
        """
        return self.get_model().objects.filter()


class UserIndex(indexes.SearchIndex, indexes.Indexable):

    """ User search index """
    text = indexes.CharField(
        document=True, use_template=False, model_attr='user__username')
    user_name_auto = indexes.EdgeNgramField(model_attr='user__username')

    def get_model(self):
        """ Get model method """
        return MoqarUser

    def index_queryset(self, using=None):
        """
        Index query method
        Used when the entire index for model is updated
        """
        return self.get_model().objects.filter()


class TagIndex(indexes.SearchIndex, indexes.Indexable):

    """ Tag search index """
    text = indexes.CharField(
        document=True, use_template=False, model_attr='text')
    tag_text_auto = indexes.EdgeNgramField(model_attr='text')

    def get_model(self):
        """ Get model method """
        return Tag

    def index_queryset(self, using=None):
        """
        Index query method
        Used when the entire index for model is updated
        """
        return self.get_model().objects.filter()
