# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from allauth.account.signals import user_signed_up
from django.dispatch import receiver

User._meta.get_field('email')._unique = True
User._meta.get_field('email').blank = False


# Create your models here.
class MoqarUser(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    user = models.OneToOneField(User)
    gender = models.CharField(
        max_length=1, choices=GENDER_CHOICES, blank=True, null=False)
    date_of_birth = models.DateField(blank=False, null=True)
    location = models.CharField(max_length=100, blank=True, null=False)
    profile_picture = models.ImageField(
        upload_to='profile_pictures/', null=True, blank=True)
    follows = models.ManyToManyField(
        'self', symmetrical=False, related_name='is_followed_by', blank=True, null=True)

    def __unicode__(self):
        return str(self.user)


class Moq(models.Model):
    question = models.CharField(max_length=200, blank=False, null=False)
    moqar_user = models.ForeignKey(MoqarUser)

    def __unicode__(self):
        return self.question


class MoqarUsersGroup(models.Model):
    name = models.CharField(max_length=30, blank=True, null=False)
    created_by = models.ForeignKey(MoqarUser, related_name='moqar_group')
    moqar_users = models.ManyToManyField(
        MoqarUser, related_name='belong_to_moqar_groups')

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = (("name", "created_by"),)


class Comment(models.Model):
    text = models.TextField(blank=False, null=False)
    written_by = models.ForeignKey(MoqarUser)
    moq_image = models.ForeignKey('MoqImage')

    def __unicode__(self):
        return self.text


class Tag(models.Model):
    text = models.CharField(max_length=30, blank=False, null=False)
    describes = models.ManyToManyField(Moq, null=True, blank=True)
    followed_by = models.ManyToManyField(
        MoqarUser, related_name='tags_followed', null=True, blank=True)

    def __unicode__(self):
        return self.text


class MoqImage(models.Model):
    IMAGE_NUMBER = (
                   (1, 'First'),
                   (2, 'Second'),
                   (3, 'Third'),
                   (4, 'Fourth'),)
    moq = models.ForeignKey(Moq)
    image_number = models.IntegerField(choices=IMAGE_NUMBER)
    image = models.ImageField(upload_to='moq_images/')
    rating = models.FloatField(default=0.0)
    description = models.TextField(blank=True, null=False)

    def __unicode__(self):
        return str(self.image_number) + ": " + str(self.moq)

    class Meta:
        unique_together = (("moq", "image_number"),)


class InvitationCodes(models.Model):
    email = models.EmailField(max_length=254, unique=True, blank=False, null=False)
    invitation_code = models.CharField(max_length=100, blank=True, null=False)
    expired = models.BooleanField(default=False)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name_plural = "Invitation codes"


@receiver(user_signed_up)
def do_after_user_signed_up(sender, **kwargs):
    user = kwargs['user']
    # Create MoqarUser and associate it with our user
    MoqarUser(user_id=user.id).save()
