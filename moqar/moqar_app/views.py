# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
import ipdb

def home(request):
    if not request.user.is_anonymous():
        return render_to_response('moqar_app/base.html', {}, RequestContext(request))
    else:
        return render_to_response('moqar_app/landing.html', {}, RequestContext(request))

def signup(request):
    return render_to_response('moqar_app/signup.html', {}, RequestContext(request))

# def complete(request):
#     if request.method == 'POST' and request.POST.get('username'):
#         name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY', 'partial_pipeline')
#         request.session['saved_username'] = request.POST['username']
#         backend = request.session['name']['backend']
#         return redirect('socialauth_complete', backend=backend)
#     return render_to_response('complete.html', {}, RequestContext(request))

def finish(request):
    print "IP Address for debug-toolbar: " + request.META['REMOTE_ADDR']
    tags = "[{'name':'Fashion'}, {'name':'Interior Design'}, {'name':'Shoes'}, {'name':'Photography'}, {'name':'Books'}, {'name':'Bags'}, {'name':'Cars'}, {'name':'Art'}]"
    return render_to_response('moqar_app/finish.html', {'tags': tags}, RequestContext(request))
